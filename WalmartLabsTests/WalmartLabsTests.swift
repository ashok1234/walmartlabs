//
//  WalmartLabsTests.swift
//  WalmartLabsTests
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import XCTest
import Combine

@testable import WalmartLabs

class WalmartLabsTests: XCTestCase {
    
    let producsApiCLient = MockRestService()
    private var disposables = Set<AnyCancellable>()

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testProductService() {
        let expectation = self.expectation(description: "got the data");
        
        producsApiCLient.getproducts()
            .receive(on: DispatchQueue.main)
            .sink(
              receiveCompletion: { [weak self] value in
                guard self != nil else { return }
                switch value {
                case .failure:
                XCTFail("network error")
                    expectation.fulfill()
                case .finished:
                  break
                }
              },
              receiveValue: { [weak self] products in
                guard self != nil else { return }
                XCTAssertNotNil(products)
                XCTAssertEqual(products.pageNumber, 1)
                expectation.fulfill()
            })
            .store(in: &disposables)
        
        self.waitForExpectations(timeout: 5.0, handler: nil)
    }

}

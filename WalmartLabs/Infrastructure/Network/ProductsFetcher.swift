/// Copyright (c) 2019 Razeware LLC
/// 
/// Permission is hereby granted, free of charge, to any person obtaining a copy
/// of this software and associated documentation files (the "Software"), to deal
/// in the Software without restriction, including without limitation the rights
/// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
/// copies of the Software, and to permit persons to whom the Software is
/// furnished to do so, subject to the following conditions:
/// 
/// The above copyright notice and this permission notice shall be included in
/// all copies or substantial portions of the Software.
/// 
/// Notwithstanding the foregoing, you may not use, copy, modify, merge, publish,
/// distribute, sublicense, create a derivative work, and/or sell copies of the
/// Software in any work that is designed, intended, or marketed for pedagogical or
/// instructional purposes related to programming, coding, application development,
/// or information technology.  Permission for such use, copying, modification,
/// merger, publication, distribution, sublicensing, creation of derivative works,
/// or sale is expressly withheld.
/// 
/// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
/// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
/// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
/// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
/// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
/// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
/// THE SOFTWARE.

import Foundation
import Combine

protocol ProductsFetchable {

    func getproducts() -> AnyPublisher<Products, ProductsError>
    func getImage(path: String) -> AnyPublisher<Data, ProductsError>?
}


public class ProductsFetcher {
  private let session: URLSession
  
  init(session: URLSession = .shared) {
    self.session = session
  }
}

// MARK: - WeatherFetchable
extension ProductsFetcher: ProductsFetchable {
    func getImage(path: String) -> AnyPublisher<Data, ProductsError>? {
        execute(with: makeGetImage(path: path))
    }
    
    func getproducts() -> AnyPublisher<Products, ProductsError> {
        return execute(with: makeGetProductsComponent())
    }
    
  private func execute<T>(
    with components: URLComponents
  ) -> AnyPublisher<T, ProductsError> where T: Decodable {
    sessionforc()
    guard let url = components.url else {
      let error = ProductsError.network(description: "Couldn't create URL")
      return Fail(error: error).eraseToAnyPublisher()
    }
    return session.dataTaskPublisher(for: URLRequest(url: url))
      .mapError { error in
        .network(description: error.localizedDescription)
      }
        .flatMap() { pair in
        return decode(pair.data)
      }
      .eraseToAnyPublisher()
  }
    
//    private func executeImage<T>(
//      with components: URLComponents
//    ) -> AnyPublisher<T, ProductsError> where T: Decodable {
//  //    sessionforc()
//      guard let url = components.url else {
//        let error = ProductsError.network(description: "Couldn't create URL")
//        return Fail(error: error).eraseToAnyPublisher()
//      }
//      return session.dataTaskPublisher(for: URLRequest(url: url))
//        .mapError { error in
//          .network(description: error.localizedDescription)
//        }
//          .flatMap() { pair in
//          return decode(pair.data)
//        }
//        .eraseToAnyPublisher()
//    }
}

func sessionforc() {
    let url = URL(string: "https://mobile-tha-server.firebaseapp.com/walmartproducts/1/20")!
    var request = URLRequest(url: url)
    var session = URLSession.shared
    let task = session.dataTask(with: request) { (data, response, error) in

        do {
            let jsondecoder = JSONDecoder()
            let response = try jsondecoder.decode(Products.self, from: data!)
            print(response)
        } catch {
            print(error) //here.....
        }

    }

    task.resume()
}

// MARK: - OpenWeatherMap API
private extension ProductsFetcher {
    func makeGetProductsComponent() -> URLComponents {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "mobile-tha-server.firebaseapp.com"
       components.path = "/walmartproducts/1/30"
        
        return components
    }
    
    func makeGetImage(path : String) -> URLComponents {
        
        var components = URLComponents()
        components.scheme = "https"
        components.host = "mobile-tha-server.firebaseapp.com"
       components.path = path
        
        return components
    }
}

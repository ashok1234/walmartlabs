
import Foundation

enum ProductsError: Error {
  case parsing(description: String)
  case network(description: String)
}

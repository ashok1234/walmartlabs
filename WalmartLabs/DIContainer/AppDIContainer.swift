
import Foundation

final class AppDIContainer {
    
//    lazy var appConfiguration = AppConfiguration()
    
    // MARK: - Network
    lazy var apiDataTransferService: ProductsFetchable = {
        return ProductsFetcher()
    }()

    
    // MARK: - DIContainers of scenes
    func makeProductsSceneDIContainer() -> ProductsSceneDIContainer {
        let dependencies = ProductsSceneDIContainer.Dependencies(apiDataTransferService: apiDataTransferService)
        return ProductsSceneDIContainer(dependencies: dependencies)
    }
}

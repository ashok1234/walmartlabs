//
//  ProductsSceneDIContainer.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import UIKit

final class ProductsSceneDIContainer {
    
    struct Dependencies {
        let apiDataTransferService: ProductsFetchable
//        let imageDataTransferService: DataTransferService
    }
    
    private let dependencies: Dependencies

    init(dependencies: Dependencies) {
        self.dependencies = dependencies
    }
    
    // MARK: - Use Cases
    func makeProductListUseCase() -> ProductListUseCase {
        return DefaultProductListUseCase(productsRepository: makeProductsRepository())
    }

    // MARK: - Repositories
    func makeProductsRepository() -> ProductsRepository {
        return DefaultProductsRepository(dataTransferService: dependencies.apiDataTransferService)
    }
    

    func makeProductListViewController(actions: ProductListViewModelActions) -> ProductListViewController {

        return ProductListViewController.create(with: makeProductListViewModel(actions: actions), imagesRepository: makePosterImagesRepository())
    }
    

    func makeProductListViewModel(actions: ProductListViewModelActions) -> ProductListViewModel {
        return ProductListViewModel(productListUseCase: makeProductListUseCase(),
                                          actions: actions)
    }
    
    // MARK: - product Details
    func makeProductDetailsViewController(product: Product) -> UIViewController {
        return ProductDetailsViewController.create(with: makeProductDetailsViewModel(product: product))
    }
    
    func makeProductDetailsViewModel(product: Product) -> ProductDetailsViewModel {
        return ProductDetailsViewModel(productItem: product)
    }
    
    func makePosterImagesRepository() -> ImageRepository {
        return DefaultPosterImagesRepository(dataTransferService: dependencies.apiDataTransferService)
    }
    
    // MARK: - Flow Coordinators
    func makeProductListFlowCoordinator(navigationController: UINavigationController) -> ProductListFlowCoordinator {
        return ProductListFlowCoordinator(navigationController: navigationController,
                                           dependencies: self)
    }
}

extension ProductsSceneDIContainer: ProductListFlowCoordinatorDependencies {}

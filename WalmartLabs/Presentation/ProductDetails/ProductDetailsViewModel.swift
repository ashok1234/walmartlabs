//
//  ProductDetailsViewModel.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import UIKit

class ProductDetailsViewModel: ObservableObject {
    
    var product : Product!
    
    init(productItem: Product) {
        product = productItem
    }
    
}

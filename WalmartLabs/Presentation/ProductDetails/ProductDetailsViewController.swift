//
//  ProductDetailsViewController.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import UIKit
import Combine

class ProductDetailsViewController: UIViewController, StoryboardInstantiable{
    
    
    @IBOutlet private var posterImageView: UIImageView!
    @IBOutlet private var overviewTextView: UILabel!
    
    var viewModel: ProductDetailsViewModel!;
    private var subscriber: AnyCancellable?
    
    static func create(with viewModel: ProductDetailsViewModel) -> ProductDetailsViewController {
        let view = ProductDetailsViewController.instantiateViewController()
        view.viewModel = viewModel
        return view
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        overviewTextView.attributedText = (viewModel.product.longDescription ?? "").html2Attributed
        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        downloadImage()
    }
    

    private func downloadImage() {
        subscriber = URLSession.shared.fetchImage(for: viewModel.product.productImage,
                                                  placeholder: UIImage(named: "placeholder"))
                    .receive(on: DispatchQueue.main)
                    .assign(to: \.posterImageView.image, on: self)
    }
    
    
    deinit {
        subscriber?.cancel()
    }
    
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

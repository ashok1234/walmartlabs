
import UIKit

protocol ProductListFlowCoordinatorDependencies  {
    func makeProductListViewController(actions: ProductListViewModelActions) -> ProductListViewController
    func makeProductDetailsViewController(product: Product) -> UIViewController
}

final class ProductListFlowCoordinator {
    
    private weak var navigationController: UINavigationController?
    private let dependencies: ProductListFlowCoordinatorDependencies

    private weak var productListVC: UIViewController?

    init(navigationController: UINavigationController,
         dependencies: ProductListFlowCoordinatorDependencies) {
        self.navigationController = navigationController
        self.dependencies = dependencies
    }
    
    func start() {
        // Note: here we keep strong reference with actions, this way this flow do not need to be strong referenced
        let actions = ProductListViewModelActions(showProductDetails: showProductDetails)
        let vc = dependencies.makeProductListViewController(actions: actions)
        
        navigationController?.pushViewController(vc, animated: true)
        productListVC = vc
    }

    private func showProductDetails(product: Product) {
        let vc = dependencies.makeProductDetailsViewController(product: product)
        navigationController?.pushViewController(vc, animated: true)
    }
}

//
//  MoviesListItemCell.swift
//  ExampleMVVM
//
//  Created by Oleh Kudinov on 01.10.18.
//

import UIKit
import Combine

final class ProductListItemCell: UITableViewCell {

    static let reuseIdentifier = String(describing: ProductListItemCell.self)
    static let height = CGFloat(130)

    @IBOutlet private var titleLabel: UILabel!
    @IBOutlet private var priceLabel: UILabel!
    @IBOutlet private var shortDescriptionLabel: UILabel!
    @IBOutlet private var posterImageView: UIImageView!
    
    private var posterImagesRepository: ImageRepository?
    private var product : Product?
    private var subscriber: AnyCancellable?


//    private var posterImagesRepository: PosterImagesRepository?
    private var imageLoadTask: Cancellable? { willSet { imageLoadTask?.cancel() } }
    
    override func prepareForReuse()
       {
           super.prepareForReuse()
           subscriber?.cancel()
       }
    

    func fill(with product: Product, posterImagesRepository: ImageRepository?) {
        self.product  = product
//        self.viewModel = viewModel
        self.posterImagesRepository = posterImagesRepository
        titleLabel.text = product.productName
        priceLabel.text = product.price
        shortDescriptionLabel.attributedText = (product.shortDescription ?? "")?.html2Attributed
        updatePosterImage(path: product.productImage)
    }
    
    private func updatePosterImage(path: String?) {
        posterImageView.image = nil
        guard let path = path else {
            return
        }
        
        subscriber = URLSession.shared.fetchImage(for: path,
                                                  placeholder: UIImage(named: "placeholder"))
                    .receive(on: DispatchQueue.main)
                    .assign(to: \.posterImageView.image, on: self)
//        guard let posterImagePath = path else { return }
//        print(posterImagesRepository)
//        posterImagesRepository?.fetchImage(with: posterImagePath, completion: { [weak self] result in
//            guard let self = self else { return }
//            guard self.product?.productImage == posterImagePath else { return }
//            switch result {
//                case .success(let data):
//                    self.posterImageView.image = UIImage(data: data)
//                case .failure(let error):
//                    print(error.localizedDescription)
//            }
//        })
    }
    
//    func featchProducts( completion: @escaping () -> ()) {
//        productListUseCase.execute(completion: { result in
//
//            switch result {
//                case .success(let products):
//                    self.dataSource = products.products;
//                    print("\(products) unread messages.")
//                    completion()
//                case .failure(let error):
//                    print(error.localizedDescription)
//            }
//        })
//    }

//    private func updatePosterImage(width: Int) {
//        posterImageView.image = nil
//        guard let posterImagePath = viewModel.posterImagePath else { return }
//
//        imageLoadTask = posterImagesRepository?.fetchImage(with: posterImagePath, width: width) { [weak self] result in
//            guard let self = self else { return }
//            guard self.viewModel.posterImagePath == posterImagePath else { return }
//            if case let .success(data) = result {
//                self.posterImageView.image = UIImage(data: data)
//            }
//            self.imageLoadTask = nil
//        }
//    }
}

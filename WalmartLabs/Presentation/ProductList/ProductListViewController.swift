//
//  ProductListViewController.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/26/21.
//

import UIKit

class ProductListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, StoryboardInstantiable{
    
    @IBOutlet var tableView: UITableView!
    
    var viewModel: ProductListViewModel?
    var posterImagesRepository: ImageRepository?
    
    static func create(with viewModel: ProductListViewModel,
                       imagesRepository: ImageRepository?) -> ProductListViewController {
        let view = ProductListViewController.instantiateViewController()
        view.viewModel = viewModel
        view.posterImagesRepository = imagesRepository
        print("creating")
        return view
    }
   
//    required init(viewModel : ProductListViewModel) {
//        self.viewModel = viewModel
//        super.init(nibName: nil, bundle: nil)
//    }
////
//    required init?(coder: NSCoder) {
//        super.init(nibName: nil, bundle: nil)
//    }
    
  
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel?.dataSource?.count ?? 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: ProductListViewModel.reuseIdentifier,
                                                       for: indexPath) as? ProductListItemCell else {
            assertionFailure("Cannot dequeue reusable cell \(ProductListViewModel.self) with reuseIdentifier: \(ProductListViewModel.reuseIdentifier)")
            return UITableViewCell()
        }
        
        guard let product = viewModel?.dataSource?[indexPath.row] else {
            return UITableViewCell()
        }

        cell.fill(with: product, posterImagesRepository: posterImagesRepository)

//        if indexPath.row == viewModel.items.value.count - 1 {
//            viewModel.didLoadNextPage()
//        }

        return cell
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.viewModel?.featchProducts(completion: {[weak self] in
            self?.tableView.reloadData()
        })
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        viewModel?.didSelectItem(at: indexPath.row)
    }
    
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

//
//  ProductListViewModel.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import Foundation
import Combine

struct ProductListViewModelActions {
    let showProductDetails: (Product) -> Void
}

class ProductListViewModel {
var dataSource: [Product]?
    static let reuseIdentifier = String(describing: ProductListItemCell.self)
    
    private let productListUseCase: ProductListUseCase
    private let actions: ProductListViewModelActions?
    

    // MARK: - Init

    init(productListUseCase: ProductListUseCase,
         actions: ProductListViewModelActions? = nil) {
        self.productListUseCase = productListUseCase
        self.actions = actions
    }
    
    
    func featchProducts( completion: @escaping () -> ()) {
        productListUseCase.execute(completion: { result in
           
            switch result {
                case .success(let products):
                    self.dataSource = products.products;
                    completion()
                case .failure(let error):
                    print(error.localizedDescription)
            }
        })
    }
    
    func didSelectItem(at index: Int) {
        guard let product = dataSource?[index] else {
            return
        }
        actions?.showProductDetails(product)
    }
    

//  func fetchWeather() {
//    weatherFetcher.getproducts()
//      .receive(on: DispatchQueue.main)
//      .sink(
//        receiveCompletion: { [weak self] value in
//          guard let self = self else { return }
//          switch value {
//          case .failure:
//            self.dataSource = nil
//          case .finished:
//            break
//          }
//        },
//        receiveValue: { [weak self] forecast in
//          guard let self = self else { return }
//          self.dataSource = forecast
//            print(self.dataSource)
//      })
//      .store(in: &disposables)
//  }
}

extension ProductListViewModel {

}

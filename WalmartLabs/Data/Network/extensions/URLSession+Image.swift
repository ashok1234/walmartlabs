//
//  File.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/26/21.
//

import UIKit
import Combine

extension URLSession
{
    func fetchImage(for url: String, placeholder: UIImage? = nil)
         -> AnyPublisher<UIImage?, Never>
    {
        var str = "https://mobile-tha-server.firebaseapp.com" + url
        let url = URL(string: str)
        return dataTaskPublisher(for: url!)
            .tryMap { data, response -> UIImage in
                guard let image = UIImage(data: data) else {
                    throw ProductsError.network(description: "error")
                }
                return image
            }
            .replaceError(with: placeholder)
            .eraseToAnyPublisher()
    }
}

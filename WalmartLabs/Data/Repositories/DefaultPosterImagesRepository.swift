//
//  DefaultPosterImagesRepository.swift
//  ExampleMVVM
//
//  Created by Oleh Kudinov on 01.10.18.
//

import Foundation
import Combine

final class DefaultPosterImagesRepository {
    
    private let dataTransferService: ProductsFetchable
    private var disposables = Set<AnyCancellable>()
    
    init(dataTransferService: ProductsFetchable) {
        self.dataTransferService = dataTransferService
    }
}

extension DefaultPosterImagesRepository: ImageRepository {
    
    func fetchImage(with imagePath: String, completion: @escaping (Result<Data, ProductsError>) -> Void) {
        dataTransferService.getImage(path: imagePath)?
            .receive(on: DispatchQueue.main)
            .sink(
              receiveCompletion: { [weak self] value in
                guard self != nil else { return }
                switch value {
                case .failure:
                    completion(.failure(ProductsError.network(description: "networkerror")))
                case .finished:
                  break
                }
              },
              receiveValue: { [weak self] image in
                guard self != nil else { return }
                completion(.success(image))
            })
            .store(in: &disposables)
    }
    
}

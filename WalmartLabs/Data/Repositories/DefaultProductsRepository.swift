//
//  DefaultMoviesRepository.swift
//  ExampleMVVM
//
//  Created by Oleh Kudinov on 01.10.18.
//
// **Note**: DTOs structs are mapped into Domains here, and Repository protocols does not contain DTOs

import Foundation
import Combine


final class DefaultProductsRepository {

    private let dataTransferService: ProductsFetchable
    private var disposables = Set<AnyCancellable>()
    
    init(dataTransferService: ProductsFetchable) {
        self.dataTransferService = dataTransferService
    }
}

extension DefaultProductsRepository: ProductsRepository, ObservableObject {
   
    func fetchProductsList(completion: @escaping (Result<Products, ProductsError>) -> Void) {
        
        dataTransferService.getproducts()
            .receive(on: DispatchQueue.main)
            .sink(
              receiveCompletion: { [weak self] value in
                guard self != nil else { return }
                switch value {
                case .failure:
                    completion(.failure(ProductsError.network(description: "networkerror")))
                case .finished:
                  break
                }
              },
              receiveValue: { [weak self] products in
                guard self != nil else { return }
                completion(.success(products))
            })
            .store(in: &disposables)
        
    }
    
}

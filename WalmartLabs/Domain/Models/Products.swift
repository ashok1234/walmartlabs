//
//  Products.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import Foundation

struct Products : Decodable {
    var pageNumber : Int
    var pageSize : Int
   var products : [Product]
    var totalProducts : Int
    var statusCode : Int
}

struct Product : Decodable {
    var productId : String
    var productName : String
    var shortDescription : String?
    var longDescription : String?
    var price : String
    var productImage : String
    var reviewRating : Float
    var reviewCount : Int
    var inStock : Bool
}

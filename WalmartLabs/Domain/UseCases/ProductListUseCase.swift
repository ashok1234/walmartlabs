//
//  ProductListUseCase.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import Foundation

protocol ProductListUseCase {
    func execute(completion: @escaping (Result<Products, ProductsError>) -> Void)
}

final class DefaultProductListUseCase: ProductListUseCase {

    private let productsRepository: ProductsRepository

    init(productsRepository: ProductsRepository) {
        self.productsRepository = productsRepository
    }

    func execute(completion: @escaping (Result<Products, ProductsError>) -> Void) {

        return productsRepository.fetchProductsList(                                                completion: { result in
//
//            if case .success = result {
//                self.moviesQueriesRepository.saveRecentQuery(query: requestValue.query) { _ in }
//            }

            completion(result)
        })
    }
}

//
//  ImageRepository.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/26/21.
//

import Foundation

protocol ImageRepository {
    func fetchImage(with imagePath: String,  completion: @escaping (Result<Data, ProductsError>) -> Void)
}

//
//  ProductsRespository.swift
//  WalmartLabs
//
//  Created by ashok kumar chejarla on 3/25/21.
//

import Foundation

protocol ProductsRepository {
    @discardableResult
    func fetchProductsList(completion: @escaping (Result<Products, ProductsError>) -> Void)
}
